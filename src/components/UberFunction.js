import React from 'react';
import { formatNumber } from '../helpers/formatNumber';

function UberFunction(props) {
  const {
    onChangeTripCount,
    onChangeMinimumRating,
    onSimulate,
    onStopSimulation
  } = props;

  const { trips, minimumRating, averageRatings, simulating } = props.stats;

  return (
    <div className="square">
      <h1>Uber Function</h1>

      <label>
        Corridas:{' '}
        <input
          type="number"
          min="0"
          value={trips}
          onChange={event => onChangeTripCount(event.target.value)}
          disabled
        />
      </label>

      <label>
        Nota mínima:{' '}
        <input
          type="number"
          min="1"
          max="5"
          value={minimumRating}
          onChange={event => onChangeMinimumRating(event.target.value)}
        />
      </label>

      <label>
        Média de avaliações:{' '}
        <input type="text" value={formatNumber(averageRatings)} disabled />
      </label>

      <div className="buttons">
        <a
          href="/#"
          className="waves-effect waves-light btn button"
          onClick={onSimulate}
          disabled={simulating}
        >
          Simular corridas
        </a>
        <a
          href="/#"
          className="waves-effect waves-light btn button"
          onClick={onStopSimulation}
          disabled={!simulating}
        >
          Parar simulação
        </a>
      </div>
    </div>
  );
}

export default UberFunction;
