import React, { useState } from 'react';
import { formatNumber } from '../helpers/formatNumber';
import { useEffect } from 'react';
import { randomNumber } from '../helpers/randomNumber';

function UberHooks() {
  /**
   * Criando estado com useState para cada
   * dado a ser "observado"
   *
   * Com useState, definimos um valor inicial
   */
  const [trips, setTrips] = useState(0);
  const [minimumRating, setMinimumRating] = useState(1);
  const [totalRatings, setTotalRatings] = useState(0);
  const [simulating, setSimulating] = useState(false);

  /**
   * Efeito para o timer de simulação
   */
  useEffect(() => {
    /**
     * Guardando a referência de interval
     * para ser eliminada posteriormente
     */
    const interval = setInterval(() => {
      /**
       * A execução depende de "simulating"
       */
      if (simulating) {
        /**
         * Usando "setState" de forma alternativa,
         * com o valor antigo como parâmetro
         */
        setTrips(oldTrips => oldTrips + 1);
      } else {
        /**
         * Usando "setState" de forma tradicional,
         * com um novo valor
         */
        setTrips(0);
      }
    }, 1); // Execução a cada 1 milisegundo

    /**
     * Isso é feito durante a "desmontagem"
     * do componente
     */
    return () => clearInterval(interval);
  }, [simulating]); // Indicamos que o effect "monitora" o atributo "simulating"

  /**
   * Este effect monitora trips e minimumRating,
   * ou seja, é disparado quando houver mudança
   * nessas variáveis de estado
   */
  useEffect(() => {
    if (trips <= 0) {
      return;
    }

    const newRatings = randomNumber(minimumRating, 5);
    setTotalRatings(oldRatings => oldRatings + newRatings);
  }, [trips, minimumRating]);

  /**
   * Ação para habilitar a simulação
   */
  const handleSimulation = () => {
    setSimulating(true);
  };

  /**
   * Ação para desabilitar a simulação
   */
  const handleStopSimulation = () => {
    setSimulating(false);
  };

  /**
   * Renderização do componente
   */
  return (
    <div className="square">
      <h1>Uber Hooks</h1>

      <label>
        Corridas: <input type="number" min="0" value={trips} disabled />
      </label>

      <label>
        Nota mínima:{' '}
        <input
          type="number"
          min="1"
          max="5"
          value={minimumRating}
          onChange={event => setMinimumRating(+event.target.value)}
        />
      </label>

      <label>
        Média de avaliações:{' '}
        <input
          type="text"
          value={formatNumber(trips === 0 ? 0 : totalRatings / trips)}
          disabled
        />
      </label>

      <div className="buttons">
        <a
          href="/#"
          className="waves-effect waves-light btn button"
          onClick={handleSimulation}
          disabled={simulating}
        >
          Simular corridas
        </a>
        <a
          href="/#"
          className="waves-effect waves-light btn button"
          onClick={handleStopSimulation}
          disabled={!simulating}
        >
          Parar simulação
        </a>
      </div>
    </div>
  );
}

export default UberHooks;
