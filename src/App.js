/**
 * React
 */
import React, { Component } from 'react';

/**
 * Componentes
 */
import UberFunction from './components/UberFunction';
import UberHooks from './components/UberHooks';

/**
 * Funções utilitárias
 */
import { randomNumber } from './helpers/randomNumber';

/**
 * CSS
 */
import './App.css';

/**
 * Componente de classe
 */
export default class App extends Component {
  /**
   * Construtor
   */
  constructor() {
    /**
     * super() é obrigatório
     */
    super();

    /**
     * Definindo o state inicial
     */
    this.state = {
      trips: 0,
      totalRatings: 0,
      averageRatings: 0,
      minimumRating: 1,
      simulating: false
    };

    /**
     * Atributo de objeto para
     * guardar a referência de interval
     */
    this.interval = null;
  }

  /**
   * Ciclo de vida que é disparado após
   * algum "setState"
   */
  componentDidUpdate(_, prevState) {
    const { trips } = this.state;

    /**
     * Evitando render desnecessário
     */
    if (trips === prevState.trips) {
      return;
    }

    /**
     * Isolando o cálculo
     */
    this.calculate();
  }

  /**
   * Cálculo de notas + média a partir
   * da geração aleatória de notas a
   * cada nova corrida
   */
  calculate() {
    const { trips, totalRatings, minimumRating } = this.state;

    if (trips <= 0) {
      return;
    }

    const newRatings = totalRatings + randomNumber(minimumRating, 5);
    const newAverage = newRatings / trips;

    /**
     * Reaproveitando o objeto original
     * e incorporando os valores recém
     * calculados
     */
    const newState = {
      ...this.state,
      totalRatings: newRatings,
      averageRatings: newAverage
    };

    this.setState(newState);
  }

  /**
   * Atualizando o state para trips
   */
  handleTripCount = newTripCount => {
    this.setState({ trips: +newTripCount });
  };

  /**
   * Atualizando o state para minimumRating
   */

  handleMinimumRating = newMinimumRating => {
    this.setState({ minimumRating: +newMinimumRating });
  };

  /**
   * Habilitando a simulação de corridas
   */
  handleSimulation = () => {
    this.setState({ simulating: true });

    this.interval = setInterval(() => {
      this.setState({ trips: this.state.trips + 1 });
    }, 1);
  };

  /**
   * Parando a simulação de corridas
   */
  handleStopSimulation = () => {
    clearInterval(this.interval);

    this.setState({
      simulating: false,
      trips: 0,
      averageRatings: 0,
      totalRatings: 0
    });
  };

  /**
   * Renderizando o componente
   */
  render() {
    const { trips, averageRatings, minimumRating, simulating } = this.state;

    return (
      <div className="container">
        <UberFunction
          stats={{ trips, averageRatings, minimumRating, simulating }}
          onChangeTripCount={this.handleTripCount}
          onChangeMinimumRating={this.handleMinimumRating}
          onSimulate={this.handleSimulation}
          onStopSimulation={this.handleStopSimulation}
        />

        <UberHooks />
        <UberHooks />
      </div>
    );
  }
}
