const formatter = new Intl.NumberFormat('pt-BR', {
  maximumSignificantDigits: 3,
  minimumSignificantDigits: 3
});

export function formatNumber(number) {
  return formatter.format(number);
}
