export function randomNumber(from = 0, to = 1000) {
  return Math.ceil(Math.max(from, Math.random() * to));
}
